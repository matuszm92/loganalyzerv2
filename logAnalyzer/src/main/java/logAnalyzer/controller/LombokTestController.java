package logAnalyzer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import logAnalyzer.model.LombokTestModel;

@Controller
public class LombokTestController {

	@RequestMapping("/")
	@ResponseBody
	public String home() {
		return new LombokTestModel("Nazwa").getName();
	}
	
}
