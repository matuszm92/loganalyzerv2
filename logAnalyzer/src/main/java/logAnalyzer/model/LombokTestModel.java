package logAnalyzer.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class LombokTestModel {

	private final String name;
	
}
